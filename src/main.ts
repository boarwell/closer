const ATTR_TRIGGER = 'data-closer-trigger';
const ATTR_TARGET = 'data-closer-target';

function removeListener(trigger: Element): void {
  trigger.removeEventListener('click', clickHandler);
}

function clickHandler(e: Event): void {
  const clickedTrigger = e.target as Element;
  const name = clickedTrigger.getAttribute(ATTR_TRIGGER);
  removeListener(clickedTrigger);

  if (name === '') {
    return;
  }

  const targets = document.querySelectorAll(`[${ATTR_TARGET}="${name}"]`);
  if (targets.length <= 0) {
    return;
  }

  targets.forEach(t => t.remove());
}

export function init(): void {
  if (document.querySelectorAll(`[${ATTR_TARGET}]`).length <= 0) {
    return;
  }

  const triggers = document.querySelectorAll(`[${ATTR_TRIGGER}]`);
  if (triggers.length <= 0) {
    return;
  }

  [...triggers].forEach(t => t.addEventListener('click', clickHandler));
}
